import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {
  @Input() width: number;
  @Input() radius: number;
  @Input() src: string;
  @Input() type: 'Circle' | 'Square';
  randomBackgroundColor: string;

  constructor() { }

  ngOnInit(): void {
  }

  getRandomColor(): string {
    return this.randomBackgroundColor ? this.randomBackgroundColor : this.generateRandomColor();
  }

  generateRandomColor(): string {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++){
      color += letters[Math.floor(Math.random() * 16)];
    }

    if (color === '#FFFFFF') {
      color = this.generateRandomColor();
    }
    this.randomBackgroundColor = color;

    return color;
  }

  getKiwiColor(): string{
    let hex = JSON.stringify(JSON.parse(this.randomBackgroundColor));
    if (hex.indexOf('#') === 0) {
      hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
      throw new Error('Invalid HEX color.');
    }
    const r = parseInt(hex.slice(0, 2), 16);
    const g = parseInt(hex.slice(2, 4), 16);
    const b = parseInt(hex.slice(4, 6), 16);

    return (r * 0.299 + g * 0.587 + b * 0.114) > 186
      ? '#000000'
      : '#FFFFFF';
  }

}
