import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomePageComponent} from './pages/user-page/children/home-page/home-page.component';
import {ProfileComponent} from './pages/user-page/children/profile/profile.component';
import {UserPageComponent} from './pages/user-page/user-page.component';

const routes: Routes = [
  {path: '', component: UserPageComponent, children: [
      {path: '', component: HomePageComponent},
      {path: 'profile', component: ProfileComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
